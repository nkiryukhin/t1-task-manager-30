package ru.t1.nkiryukhin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.EmailEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.LoginEmptyException;
import ru.t1.nkiryukhin.tm.model.User;


public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password) throws AbstractException;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email) throws AbstractException;

    User create(String login, String password, Role role) throws AbstractException;

    @Nullable
    User findByLogin(@Nullable String login) throws LoginEmptyException;

    @Nullable
    User findByEmail(@Nullable String email) throws EmailEmptyException;

    @Nullable
    User removeByLogin(@Nullable String login) throws AbstractException;

    @Nullable
    User removeByEmail(@Nullable String email) throws AbstractException;

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password) throws AbstractException;

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    ) throws AbstractException;

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login) throws AbstractException;

    void unlockUserByLogin(@Nullable String login) throws AbstractException;

}