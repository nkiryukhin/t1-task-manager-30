package ru.t1.nkiryukhin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.model.Task;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "list task by project id";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list-by-project-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("[ENTER PROJECT ID:]");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @NotNull final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        renderTasks(tasks);
    }

}
