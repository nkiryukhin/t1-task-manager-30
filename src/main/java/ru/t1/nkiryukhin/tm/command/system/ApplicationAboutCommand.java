package ru.t1.nkiryukhin.tm.command.system;


import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show developer info";
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @Override
    public void execute() {
        @NotNull final IPropertyService service = getPropertyService();
        System.out.println("[ABOUT]");
        System.out.println("AUTHOR: " + getPropertyService().getAuthorName());
        System.out.println("E-MAIL: " + getPropertyService().getAuthorEmail());
        System.out.println();

        System.out.println("[GIT]");
        System.out.println("BRANCH: " + service.getGitBranch());
        System.out.println("COMMIT ID: " + service.getGitCommitId());
        System.out.println("COMMITTER " + service.getGitCommitterName());
        System.out.println("E-MAIL: " + service.getGitCommitterEmail());
        System.out.println("MESSAGE: " + service.getGitCommitMessage());
        System.out.println("TIME: " + service.getGitCommitTime());
    }

}